# Hotspot Helper

Bash script for logging in and logging out into captive portals (hotspots).

> Currently only MikroTik RouterOS (UserManager package) is supported

## Usage:
First add executable permission:
```bash
    chmod +x hotspot.sh
```

**In order to log in:**

```bash
    ./hotspot.sh
```
or
```bash
    ./hotspot.py
```
**To log out:**
```bash
    ./hotspot.sh -o
```
or
```bash
    ./hotspot.py -o
```

## License
Created by Mohammad-Reza Daliri, released under MIT License.
Python script added by Ali Goldani.