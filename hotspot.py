import argparse
import requests
import hashlib

url = "https://hotspot.um.ac.ir/"

def login():
    username = "Enter your username here"
    password = "Enter you password here"
    password_md5_encoded = hashlib.md5(password.encode())

    payload = "username=" + username + "&password=" + password_md5_encoded.hexdigest() + "&dst=&popup=true"
    headers = {
        'Host': "hotspot.um.ac.ir",
        'Content-Length': str(len(payload)),
        'Content-Type': "application/x-www-form-urlencoded"
        }

    response = requests.request("POST", url + "/login", data=payload, headers=headers)

    if response.status_code == 200:
        print("Login successfull!")

def logout():
    headers = {'Host': "hotspot.um.ac.ir"}
    response = requests.request("POST", url + "/logout", headers=headers)

    if response.status_code == 200:
        print("Logged out successfully!")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Use for login/logout from "https://hotspot.um.ac.ir".')
    parser.add_argument('-o', help='Logout flag', action='store_true')
    args = parser.parse_args()
    if not args.o:
        login()
    else:
        logout()
